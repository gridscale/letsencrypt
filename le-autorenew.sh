#!/bin/bash

web_service='haproxy'
haproxy_ca="/etc/haproxy/cert/"
le_ca="/etc/letsencrypt/live"

le_path='/opt/letsencrypt'
exp_limit=90;

# Go through all domains and check if certificates need to renew
for domain in $( ls -1 $le_ca ) ;do
	cert_file="$le_ca/$domain/fullchain.pem"
	key_file="$le_ca/$domain/privkey.pem"
	echo $cert_file
	echo $key_file

	if [ -f $cert_file ] && [ -f $key_file ]; then

		# check expire date
		exp=$(date -d "`openssl x509 -in $cert_file -text -noout|grep "Not After"|cut -c 25-`" +%s)
		# what is now?
		datenow=$(date -d "now" +%s)
		# how many days until CA expires?
		days_exp=$(echo \( $exp - $datenow \) / 86400 |bc)
	
		echo "Checking expiration date for $domain..."
	
		if [ "$days_exp" -gt "$exp_limit" ]; then
			echo "[NOTE] nothing to do, Cert is up to day. Renewal in $days_exp."
		else
			echo "[NOTE] will try to renew Cert for $domain"
			# Let's check, if multiple CN are used
			ca_domains=$(openssl x509 -in /etc/letsencrypt/live/$domain/fullchain.pem -text -noout |grep 'DNS:' |sed -r -e 's/DNS:/--domains /g' |sed -r -e 's/,//g')
			
			# Build Renew-Command
			renew="$le_path/letsencrypt-auto certonly --renew-by-default --http-01-port 60001 --agree-tos $ca_domains"
	
			# Execute command
			$renew
	
			if [ $? == 0 ]; then
				# Cert was regenerated, let's build the new HAProxy Cert
				mv $haproxy_ca/$domain.pem $haproxy_ca/$domain.pem.old
				cat $cert_file $key_file > $haproxy_ca/$domain.pem
			fi
			echo "Reloading $web_service"
			/usr/sbin/service $web_service reload
			if [ $? == 0 ]; then
				echo "Renewal process finished for domain $domain"
				exit 0;
			else
				echo "Something failed, try to revert the CA-changes and reload haproxy"
				mv $haproxy_ca/$domain.pem.old $haproxy_ca/$domain.pem
				/usr/sbin/service $web_service reload
			fi
		fi
	else
		echo "[ERR] Keyfile or CA File does not exist. Skipping domain $domain"
	fi
done
